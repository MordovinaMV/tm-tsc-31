package ru.tsc.mordovina.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.mordovina.tm.api.IRepository;
import ru.tsc.mordovina.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findUserByLogin(@Nullable String login);

    @Nullable
    User findUserByEmail(@NotNull String email);

    @Nullable
    User removeUserById(@NotNull String id);

    @Nullable
    User removeUserByLogin(@NotNull String login);

    boolean userExistsByLogin(@NotNull String login);

    boolean userExistsByEmail(@NotNull String email);

}
