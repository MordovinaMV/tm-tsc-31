package ru.tsc.mordovina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.mordovina.tm.api.repository.IUserRepository;
import ru.tsc.mordovina.tm.model.User;

import java.util.function.Predicate;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    public static Predicate<User> predicateByEmail(@NotNull final String email) {
        return u -> email.equals(u.getEmail());
    }

    @Nullable
    @Override
    public User findUserByLogin(@Nullable final String login) {
        return list.stream()
                .filter(u -> u.getLogin().toLowerCase().equals(login.toLowerCase()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User findUserByEmail(@NotNull final String email) {
        return list.stream()
                .filter(predicateByEmail(email))
                .limit(1)
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public User removeUserById(@NotNull final String id) {
        @NotNull final User user = findById(id);
        list.remove(user);
        return user;
    }

    @Nullable
    @Override
    public User removeUserByLogin(@NotNull final String login) {
        @Nullable final User user = findUserByLogin(login);
        if (user == null) return null;
        list.remove(user);
        return user;
    }

    @Override
    public boolean userExistsByLogin(@NotNull final String login) {
        return findUserByLogin(login) != null;
    }

    @Override
    public boolean userExistsByEmail(@NotNull final String email) {
        return findUserByEmail(email) != null;
    }

}
